//
// Created by Csurleny on 6/11/2022.
//

#ifndef INC_15_ADC_REGULAR_CIRCULAR_GAUGE_H
#define INC_15_ADC_REGULAR_CIRCULAR_GAUGE_H

#include <stdint.h>

void init_gauge(uint16_t min, uint16_t max);
void update_gauge(uint16_t val);

#endif //INC_15_ADC_REGULAR_CIRCULAR_GAUGE_H
