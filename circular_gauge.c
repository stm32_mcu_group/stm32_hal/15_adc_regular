//
// Created by Csurleny on 6/11/2022.
//

#include "circular_gauge.h"
#include "ssd1306_oled.h"
#include "math.h"

#define pi  3.14159265359
#define pi2  pi*2

static float startAngleD, startAngle;
static float endAngleD, endAngle;
static int centerX = 64, centerY = -40, radius;

static uint16_t gaugeMin, gaugeMax;

static const float centerD = 90;
static const float widthD = 45;

static float tAngle[2];

static void drawArc(float sAngle, float eAngle, float segments, float cX, float cY, int radius){
    float resolution = (eAngle-sAngle)/segments;
    uint16_t x = (uint16_t)(cX+radius*cosf(sAngle));
    uint16_t y = (uint16_t)(cY+radius*sinf(sAngle));
    SSD1306_DrawPixel(x, y, SSD1306_COLOR_WHITE);

    for(float angle=sAngle; angle < eAngle; angle += resolution){
        x = (uint16_t)(cX+radius*cosf(angle));
        y = (uint16_t)(cY+radius*sinf(angle));
        SSD1306_DrawPixel(x, y, SSD1306_COLOR_WHITE);
    }
}

static void drawGaugeLines(float sAngle, float eAngle, float cX, float cY, int radius){
    drawArc(sAngle, eAngle, 150, cX, cY, radius + 30);
    drawArc(sAngle, eAngle, 110, cX, cY, radius - 1);
    drawArc(sAngle, eAngle, 110, cX, cY, radius - 4);
}

static float angleToXD(float cX, float radius, float angleD) {
    float angle = (angleD / 360) * (pi2);
    return cX+radius*cosf(angle);
}

static float angleToYD(float cY, float radius, float angleD) {
    float angle = (angleD / 360) * (pi2);
    return cY+radius*sinf(angle);
}

static void drawGaugeNeedle(float angle, float cX, float cY, int radius, SSD1306_COLOUR_t c){
    float leftX = angleToXD(cX, radius+1, angle - 5);
    float leftY = angleToYD(cY, radius+1, angle - 5);

    float rightX = angleToXD(cX, radius+1, angle + 5);
    float rightY = angleToYD(cY, radius+1, angle + 5);

    float topX = angleToXD(cX, radius+30, angle);
    float topY = angleToYD(cY, radius+30, angle);

    SSD1306_DrawFilledTriangle(leftX, leftY, topX, topY, rightX, rightY, c);
}

static void drawValue(char* value){

}

void init_gauge(uint16_t min, uint16_t max){
    startAngleD = centerD -widthD;
    endAngleD = centerD +widthD;

    startAngle = startAngleD / 360.0f * pi2;
    endAngle = endAngleD / 360.0f * pi2;

    gaugeMin = min;
    gaugeMax = max;

    SSD1306_Init();
    drawGaugeLines(startAngle, endAngle, centerX, centerY, 65);
    //drawGaugeNeedle(90, centerX, centerY, 65, SSD1306_COLOR_WHITE);
    SSD1306_UpdateScreen();
}

void update_gauge(uint16_t val){

    tAngle[1] = tAngle[0];

    tAngle[0] = (endAngleD - startAngleD)/(float)(gaugeMin - gaugeMax)*(float)(val);
    tAngle[0] += (startAngleD*gaugeMin - endAngleD*gaugeMax)/(float)(gaugeMin - gaugeMax);

    drawGaugeNeedle(tAngle[1], centerX, centerY, 65, SSD1306_COLOR_BLACK);
    drawGaugeNeedle(tAngle[0], centerX, centerY, 65, SSD1306_COLOR_WHITE);
    drawGaugeLines(startAngle, endAngle, centerX, centerY, 65);

    SSD1306_UpdateScreen();
}